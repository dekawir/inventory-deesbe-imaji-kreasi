<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnBarang extends Model
{
    use HasFactory;
    protected $table = 'return_barang';
    protected $guarded = ['id'];
    
    public $timestamps = false;
}
