<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;

class CekLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, $roles): Response
    {
        if(!Auth::check()){
            return redirect()->route('login')->withError(['error'=>'Anda tidak punyak akses!']);
        }

        $user = Auth::user();
        // dd($user->jabatan);
        // if($user->jabatan === 'manager');{
        //     return response()->json('aa');
        // }
        // return $next($request);
 
        $user = Auth::user();
        if($user->jabatan == $roles)

        return $next($request);


        // return response()->json('Your account is inactive');
        return back()->withInput()->withErrors(['error' => 'Akses dilarang!!!']);
        // return redirect('dashboard')->withError(['error'=>'Anda tidak punyak akses!']);



    }
}
