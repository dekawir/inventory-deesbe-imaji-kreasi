<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    function index(Request $request) {
        if($request->add){

            $validator = Validator::make($request->all(),[
                'username'=>'required|unique:users',
                'password'=> 'required',
                'nama' => 'required',
                'jabatan'=>'required',
                'no_telp'=>'required|max:13',
                'alamat'=>'required',
                'email'=>'required|unique:users'
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $user = [
                'username'=> $request->username,
                'password'=> Hash::make($request->password),
                'nama' =>  $request->nama,
                'jabatan'=> $request->jabatan,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
                'email'=> $request->email
            ];
            // dd($user);
            User::create($user);

            return back()->with('success','Data Berhasil Ditambah');
        }

        if($request->edit){

            $validator = Validator::make($request->all(),[
                'username'=>'required',
                'password'=> 'required',
                'nama' => 'required',
                'jabatan'=>'required',
                'no_telp'=>'required|max:13',
                'alamat'=>'required',
                'email'=>'required'
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $user = [
                'username'=> $request->username,
                'password'=> Hash::make($request->password),
                'nama' =>  $request->nama,
                'jabatan'=> $request->jabatan,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
                'email'=> $request->email
            ];
            // dd($user);
            User::where('id',$request->id)->update($user);

            return back()->with('success','Data Berhasil Diedit');
        }

        return view('masterData.dataUser',[
            'title'=>'Data User',
            'user'=>User::all(),
            'no'=>1,

        ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return response()->json([
            'status'=> 200,
            'user' => $user
        ]);
    }

    function destroy(Request $request){
        User::where('id',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }
}
