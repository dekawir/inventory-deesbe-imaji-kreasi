<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangKeluar;
use App\Models\BarangMasuk;
use App\Models\StockOpname;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    function index(Request $request) {
        // dd(Auth::user()->id);
        if($request->add){

            $validator = Validator::make($request->all(),[
                // 'kode_barang'=>'required|unique:barang',
                'kategori'=> 'required',
                'nama_barang' => 'required|unique:barang',
                'gender'=>'required',
                'warna'=>'required',
                'size'=>'required',
                // 'stock'=>'required',
                'harga_jual'=>'required',
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $kodeBarang = $request->cat.'-'.$request->urut.'-'.$request->gen.'-'.$request->siz;
            // dd($kodeBarang);
            $data = [
                'id_user'=>Auth::user()->id,
                'kode_barang'=>$kodeBarang,
                'kategori'=> $request->kategori,
                'nama_barang' => $request->nama_barang,
                'gender'=>$request->gender,
                'warna'=>$request->warna,
                'size'=>$request->size,
                'stock'=> '0',
                'harga_jual'=>$request->harga_jual,
            ];

            // dd($data);
            
            Barang::create($data);

            return back()->with('success','Data Berhasil Ditambah');
        }
        if($request->edit){

            $validator = Validator::make($request->all(),[
                // 'kode_barang'=>'required|unique:barang',
                'kategori'=> 'required',
                'nama_barang' => 'required',
                'gender'=>'required',
                'warna'=>'required',
                'size'=>'required',
                // 'stock'=>'required',
                'harga_jual'=>'required',
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $data = [
                // 'kode_barang'=>$request->kode_barang,
                'kategori'=> $request->kategori,
                'nama_barang' => $request->nama_barang,
                'gender'=>$request->gender,
                'warna'=>$request->warna,
                'size'=>$request->size,
                // 'stock'=>$request->stock,
                'harga_jual'=>$request->harga_jual,
            ];
            
            Barang::where('kode_barang',$request->kode_barang)->update($data);

            return back()->with('success','Data Berhasil Diedit');
        }
        if(Barang::count()>=1){
            $result = Barang::select(Barang::raw('mid(kode_barang, 5, 3) as result'))->get()->last();

            $start_value = $result->result;

            $barang = str_pad((int) $start_value+1, 3 ,"0",STR_PAD_LEFT);
            // dd($barang);

            
        }else{
            $barang = '001';
            
        }

        
        return view('masterData.dataBarang',[
            'title'=>'Data Barang',
            'data'=>Barang::all(),
            'no'=>1,
            'urut'=> $barang,
            
        ]);
    }

    public function edit($id)
    {
        $data = Barang::where('id',$id)->first();
        
        return response()->json([
            'status'=> 200,
            'data' => $data,
            'validasi'=> Barang::where('id',$id)->first()->stock,
        ]);
    }

    function destroy(Request $request){
        BarangMasuk::where('id_barang',$request->id)->delete();
        BarangKeluar::where('id_barang',$request->id)->delete();
        StockOpname::where('id_barang',$request->id)->delete();


        // dd($kodeBarang);
        Barang::where('id',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }
}
