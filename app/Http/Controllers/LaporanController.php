<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LaporanController extends Controller
{
    function index(Request $request) {

        return view('laporan.laporanBarang',[
            'title'=>'Laporan Stock Barang',
            'data'=>Barang::all(),
            'no'=>1,
            
        ]);
    }
    function masuk(Request $request) {
        if($request->cari){
            $validator = Validator::make($request->all(),[
                'start'=> 'required',
                'end' => 'required',                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $start = date('Y-m-d',strtotime($request->start));
            $end = date('Y-m-d',strtotime($request->end));

            $data = Barang::join('barang_masuk','barang_masuk.id_barang','=','barang.id')
            // ->select('barang.nama_barang','barang.kode_barang','barang_masuk.*')
            ->whereBetween('tgl_masuk',[$start,$end])->get();
            $reset=1;
        }elseif($request->reset){

            $data = Barang::join('barang_masuk','barang_masuk.id_barang','=','barang.id')
            ->get();

            $reset=0;
        }else{
            $data = Barang::join('barang_masuk','barang_masuk.id_barang','=','barang.id')
            ->get();

            $reset=0;
        }


        return view('laporan.laporanBarangMasuk',[
            'title'=>'Laporan Data Barang Masuk',
            'data'=>$data,
            'no'=>1,
            'reset'=> $reset,
            
        ]);
    }
    function keluar(Request $request) {
        if($request->cari){
            $validator = Validator::make($request->all(),[
                'start'=> 'required',
                'end' => 'required',                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $start = date('Y-m-d',strtotime($request->start));
            $end = date('Y-m-d',strtotime($request->end));

            $data = Barang::join('barang_keluar','barang_keluar.id_barang','=','barang.id')
            ->join('reseller','reseller.id','=','barang_keluar.id_reseller')
            ->whereBetween('tgl_keluar',[$start,$end])->get();
            $reset=1;
        }elseif($request->reset){

            $data = Barang::join('barang_keluar','barang_keluar.id_barang','=','barang.id')
            ->join('reseller','reseller.id','=','barang_keluar.id_reseller')
            ->get();

            $reset=0;
        }else{
            $data = Barang::join('barang_keluar','barang_keluar.id_barang','=','barang.id')
            ->join('reseller','reseller.id','=','barang_keluar.id_reseller')
            // ->where('status','Approve')
            ->get();

            $reset=0;
        }


        return view('laporan.laporanBarangKeluar',[
            'title'=>'Laporan Data Barang Keluar',
            'data'=>$data,
            'no'=>1,
            'reset'=> $reset,
            
        ]);
    }
    function stockOpname(Request $request) {
        if($request->cari){
            $validator = Validator::make($request->all(),[
                'start'=> 'required',
                'end' => 'required',                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $start = date('Y-m-d',strtotime($request->start));
            $end = date('Y-m-d',strtotime($request->end));

            $data = Barang::join('stock_opname','stock_opname.id_barang','=','barang.id')
            ->join('users','users.id','=','stock_opname.id_user')
            ->where('status','Approve')
            ->whereBetween('tgl_penyesuaian',[$start,$end])
            ->get();
            $reset=1;
        }elseif($request->reset){

            $data = Barang::join('stock_opname','stock_opname.id_barang','=','barang.id')
            ->where('status','Approve')
            ->get();

            $reset=0;
        }else{
            $data = Barang::join('stock_opname','stock_opname.id_barang','=','barang.id')
            ->where('status','Approve')
            ->get();

            $reset=0;
        }


        return view('laporan.laporanStockOpname',[
            'title'=>'Laporan Stock Opname',
            'data'=>$data,
            'no'=>1,
            'reset'=> $reset,
            
        ]);
    }
}
