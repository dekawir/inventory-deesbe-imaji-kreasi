<?php

namespace App\Http\Controllers;

use App\Models\BarangKeluar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{


    public function dashboard(Request $request)
    {
        DB::statement("SET SQL_MODE=''");
        $barangKeluar = DB::select('select nama_barang, count(id_barang) as jumlah
        from barang_keluar inner join barang on barang.id=barang_keluar.id_barang
        group by id_barang order by id_barang desc
        ');
        $reseller = DB::select('select nama, count(id_reseller) as jumlah
        from barang_keluar inner join reseller on reseller.id=barang_keluar.id_reseller
        group by id_reseller order by id_reseller desc
        ');

        // dd($reseller);
        return view('dashboard',[
            'title'=>'Dashboard',
            'barang'=>$barangKeluar,
            'reseller'=>$reseller
        ]);
    }
    public function manager(){
        return 'manager';
    }
    public function admin(){
        return 'admin';
    }
}
