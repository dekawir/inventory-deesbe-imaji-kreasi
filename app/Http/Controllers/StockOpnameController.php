<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\StockOpname;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StockOpnameController extends Controller
{
    function index(Request $request){

        if($request->add){
            // dd($request->all());
            $tgl = date('Y-m-d', strtotime($request->tgl_penyesuaian));
            $check = StockOpname::where('id_barang',$request->id_barang)->where('tgl_penyesuaian',$tgl )->count();
            
            if($check>0){
                return back()->with('info','Barang sudah di stock opname per hari ini !!!');
            }

            $validator = Validator::make($request->all(),[
                'id_barang'=>'required',
                'tgl_penyesuaian'=> 'required',             
                'jumlah_sebenarnya'=> 'required',             
                'keterangan'=> 'required',             
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'id_user'=> Auth::user()->id,
                'id_barang'=> $request->id_barang,
                'tgl_penyesuaian' => date('Y-m-d', strtotime($request->tgl_penyesuaian)),
                'stock'=> $request->stock,
                'jumlah_sebenarnya'=> $request->jumlah_sebenarnya,
                'keterangan'=> $request->keterangan,
                'status'=> 'Pending',
            ];
                        
            StockOpname::create($data);
            
            return back()->with('success','Data Berhasil Ditambah');

        }
        if($request->edit){

            $validator = Validator::make($request->all(),[
                'tgl_penyesuaian'=> 'required',             
                'jumlah_sebenarnya'=> 'required',             
                'keterangan'=> 'required',             
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'tgl_penyesuaian' => date('Y-m-d', strtotime($request->tgl_penyesuaian)),
                'jumlah_sebenarnya'=> $request->jumlah_sebenarnya,
                'keterangan'=> $request->keterangan,
            ];
                        
            StockOpname::where('id',$request->id)->update($data);

            return back()->with('success','Data Berhasil Diedit');

        }

        return view('stockOpname',[
            'title'=>'Data Stockopname',
            'data'=> Barang::join('stock_opname','stock_opname.id_barang','=','barang.id')
            ->join('users','users.id','=','stock_opname.id_user')
            ->select('barang.*','stock_opname.*','users.nama as nama')
            ->where('status','Pending')
            ->get(),
            'barang'=> Barang::all(),
            'dateNow'=>Carbon::now()->format('d-m-Y H:i:s'),
        ]);
    }

    public function edit($id)
    {
        $data = Barang::join('stock_opname','stock_opname.id_barang','=','barang.id')
        ->where('stock_opname.id',$id)->first();
        
        return response()->json([
            'status'=> 800,
            'data' => $data,
        ]);
    }

    function destroy(Request $request){

        StockOpname::where('id',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }

    function approve(Request $request){

        $dataBarang = StockOpname::where('id',$request->id)->first(); 
        
        StockOpname::where('id',$request->id)->update(['status'=>'Approve']);
        Barang::where('id',$dataBarang->id_barang)->update(['stock'=>$dataBarang->jumlah_sebenarnya]);
        return back()->with('success','Data Sudah Stock Opname');
    }
}
