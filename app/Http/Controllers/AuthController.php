<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if ($user = Auth::user()) {
            if($user->jabatan == 'manager') {
                return redirect()->intended('dashboard');
            } elseif ($user->jabatan == 'admin') {
                return redirect()->intended('dashboard');
            }
        }


        if($request->post())
        {
            $validator = Validator::make($request->all(),[
                'username' => 'required',
                'password' => 'required',
            ]);
            
            if ($validator->fails()) {
                return back()->withInput()->withErrors(['error' => 'Ada field yang masih kosong!!!']);
            }

           
        $credentials = $request->only('username','password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $request->session()->regenerate();
                if ($user->jabatan == 'manager') {
                    return redirect()->intended('dashboard');
                } elseif ($user->jabatan == 'admin') {
                    return redirect()->intended('dashboard');
                }
                return redirect()->intended('/');
            }

        return redirect('login')->withInput()->withErrors(['error' => 'Username atau Password salah!!!']);

        }
        return view('auth');
        
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('login');
    }
}
