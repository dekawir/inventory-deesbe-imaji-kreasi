<?php

namespace App\Http\Controllers;

use App\Models\Reseller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResellerController extends Controller
{
    function index(Request $request) {
        if($request->add){

            $validator = Validator::make($request->all(),[
                'nama'=>'required',
                'no_ktp'=> 'required|max:16',
                'nama_toko' => 'required',
                'email'=>'required|unique:reseller',
                'no_telp'=>'required|max:13',
                'alamat'=>'required',
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $data = [
                'nama'=> $request->nama,
                'no_ktp'=> $request->no_ktp,
                'nama_toko' =>  $request->nama_toko,
                'email'=> $request->email,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
            ];
            
            Reseller::create($data);

            return back()->with('success','Data Berhasil Ditambah');
        }
        if($request->edit){

            $validator = Validator::make($request->all(),[
                'nama'=>'required|unique:reseller',
                'no_ktp'=> 'required|max:16',
                'nama_toko' => 'required',
                'email'=>'required|unique:reseller',
                'no_telp'=>'required|max:13',
                'alamat'=>'required',
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $data = [
                'nama'=> $request->nama,
                'no_ktp'=> $request->no_ktp,
                'nama_toko' =>  $request->nama_toko,
                'email'=> $request->email,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
            ];
            
            Reseller::create($data);

            return back()->with('success','Data Berhasil Diedit');
        }

        
        return view('masterData.dataReseller',[
            'title'=>'Data Reseller',
            'data'=>Reseller::all(),
            'no'=>1,

        ]);
    }

    public function edit($id)
    {
        $data = Reseller::find($id);
        return response()->json([
            'status'=> 200,
            'data' => $data
        ]);
    }

    function destroy(Request $request){
        Reseller::where('id',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }
}
