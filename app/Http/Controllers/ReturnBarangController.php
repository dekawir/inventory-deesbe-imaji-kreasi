<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangKeluar;
use App\Models\Reseller;
use App\Models\ReturnBarang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ReturnBarangController extends Controller
{
    function index(Request $request){

        if($request->add){
            // dd($request->all());
            $tgl = date('Y-m-d', strtotime($request->tgl_return));

            $validator = Validator::make($request->all(),[
                'id_barang'=>'required',
                'reseller'=>'required',
                'no_faktur'=>'required',
                'tgl_return'=> 'required',
                'jumlah_barang'=> 'required',
                'keterangan'=> 'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'id_user'=> Auth::user()->id,
                'id_barang'=> $request->id_barang,
                'id_reseller'=> $request->reseller,
                'no_faktur'=> $request->no_faktur,
                'tgl_return' => date('Y-m-d', strtotime($request->tgl_return)),
                'jumlah_barang'=> $request->jumlah_barang,
                'keterangan'=> $request->keterangan,
            ];
                        
            ReturnBarang::create($data);
            
            return back()->with('success','Data Berhasil Ditambah');

        }
        if($request->edit){

            $validator = Validator::make($request->all(),[
                'tgl_return'=> 'required',             
                'jumlah_barang'=> 'required',             
                'keterangan'=> 'required',             
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'tgl_return' => date('Y-m-d', strtotime($request->tgl_return)),
                'jumlah_barang'=> $request->jumlah_barang,
                'keterangan'=> $request->keterangan,
            ];
                        
            ReturnBarang::where('id',$request->id)->update($data);

            return back()->with('success','Data Berhasil Diedit');

        }

        return view('proses.barangReturn',[
            'title'=>'Data Return Barang',
            'data'=> Barang::join('return_barang','return_barang.id_barang','=','barang.id')
            ->select('barang.*','return_barang.*')
            ->get(),
            'barang'=> Barang::all(),
            'dateNow'=>Carbon::now()->format('d-m-Y H:i:s'),
            'reseller'=> Reseller::all(),
            'faktur'=>BarangKeluar::select('no_faktur')->get(),

        ]);
    }

    public function edit($id)
    {
        $data = Barang::join('return_barang','return_barang.id_barang','=','barang.id')
        ->where('return_barang.id',$id)->first();
        
        return response()->json([
            'status'=> 800,
            'data' => $data,
        ]);
    }

    function destroy(Request $request){

        ReturnBarang::where('id',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }

}
