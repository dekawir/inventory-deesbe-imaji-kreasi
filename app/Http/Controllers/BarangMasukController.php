<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangMasuk;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BarangMasukController extends Controller
{
    function index(Request $request){

        if($request->add){
            $validator = Validator::make($request->all(),[
                'kode_barang'=>'required',
                'tgl_masuk'=> 'required',             
                'jumlah_barang'=> 'required',             
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'id_user'=> Auth::user()->id,
                'id_barang'=> $request->kode_barang,
                'tgl_masuk' => date('Y-m-d', strtotime($request->tgl_masuk)),
                'jumlah_barang'=> $request->jumlah_barang,
            ];
            
            $lastStock = Barang::where('id',$request->kode_barang)->first()->stock;
            $tambah = $lastStock+$request->jumlah_barang;
            Barang::where('id',$request->kode_barang)->update(['stock'=>$tambah]);

            BarangMasuk::create($data);
            
            return back()->with('success','Data Berhasil Ditambah');

        }
        if($request->edit){
            // dd($request->all());
            $validator = Validator::make($request->all(),[
                // 'kode_barang'=>'required',
                'tgl_masuk'=> 'required',             
                'jumlah_barang'=> 'required',             
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                // 'kode_barang'=> $request->kode_barang,
                'tgl_masuk' => date('Y-m-d', strtotime($request->tgl_masuk)),
                'jumlah_barang'=> $request->jumlah_barang,
            ];

            $lastStock = Barang::where('id',$request->id_barang)->first()->stock;
            $lastStockIn = BarangMasuk::where('id',$request->id_barang_masuk)->first()->jumlah_barang;
            $edit = ($lastStock - $lastStockIn) + $request->jumlah_barang;
            // dd($lastStock, $lastStockIn,$edit, $request->id_barang, $request->id_barang_masuk);
            Barang::where('id',$request->id_barang)->update(['stock'=>$edit]);
            
            BarangMasuk::where('id',$request->id_barang_masuk)->update($data);

            return back()->with('success','Data Berhasil Diedit');

        }

        return view('proses.barangMasuk',[
            'title'=>'Data Barang Masuk',
            'data'=> Barang::join('barang_masuk','barang_masuk.id_barang','=','barang.id')
            ->select('barang.nama_barang','barang.kode_barang','barang_masuk.*')
            ->get(),
            'barang'=> Barang::all(),
            'no'=>1,
            'dateNow'=>Carbon::now()->format('d-m-Y H:i:s'),
        ]);
    }

    public function edit($id)
    {
        $data = Barang::join('barang_masuk','barang_masuk.id_barang','=','barang.id')
        // ->select('barang.nama_barang','barang.kode_barang','barang.kategori','barang.kode_barang','barang_masuk.*')
        ->where('barang_masuk.id',$id)->first();
        return response()->json([
            'status'=> 500,
            'data' => $data
        ]);
    }

    public function show($id)
    {
        $data = Barang::where('id',$id)->first();
        return response()->json([
            'status'=> 300,
            'data' => $data
        ]);
    }
    
    function destroy(Request $request){
        $kodeBarang = BarangMasuk::where('id',$request->id)->first()->id_barang;
        $jumlahBarang = BarangMasuk::where('id',$request->id)->first()->jumlah_barang;

        $lastStock = Barang::where('id',$kodeBarang)->first()->stock;
        $kurang = $lastStock - $jumlahBarang;
        Barang::where('id',$kodeBarang)->update(['stock'=>$kurang]);
        // dd($kurang, $kodeBarang);


        BarangMasuk::where('id',$request->id)->delete();


        return back()->with('success','Data Berhasil Dihapus');
    }
}
