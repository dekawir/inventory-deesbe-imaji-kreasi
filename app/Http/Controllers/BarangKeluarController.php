<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangKeluar;
use App\Models\Reseller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BarangKeluarController extends Controller
{
    function index(Request $request){

        if($request->add){
            $validator = Validator::make($request->all(),[
                'reseller'=>'required',
                'no_faktur'=>'required',
                'id_barang'=>'required',
                'tgl_keluar'=> 'required',             
                'jumlah_barang'=> 'required',             
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'id_user'=> Auth::user()->id,
                'id_reseller'=> $request->reseller,
                'no_faktur'=> $request->no_faktur,
                'id_barang'=> $request->id_barang,
                'tgl_keluar' => date('Y-m-d', strtotime($request->tgl_keluar)),
                'jumlah_barang'=> $request->jumlah_barang,
            ];

            // dd($request->all());
            
            $lastStock = Barang::where('id',$request->id_barang)->first()->stock;
            if($lastStock > 0){
                if($lastStock < $request->jumlah_barang){
                    return back()->with('info','Stock tidak cukup, stock tersisa hanya : '.$lastStock);
                }else{
                    $kurang = $lastStock - $request->jumlah_barang;
                    Barang::where('id',$request->id_barang)->update(['stock'=>$kurang]);
                    BarangKeluar::create($data);
                }
            }else{
                return back()->with('info','Stock masih kosong !!!');
            }

            return back()->with('success','Data Berhasil Ditambah');

        }
        if($request->edit){

            $validator = Validator::make($request->all(),[
                'reseller'=>'required',
                'no_faktur'=>'required',
                'tgl_keluar'=> 'required',             
                'jumlah_barang'=> 'required',            
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }
            
            $data = [
                'id_reseller'=> $request->reseller,
                'no_faktur'=> $request->no_faktur,
                'tgl_keluar' => date('Y-m-d', strtotime($request->tgl_keluar)),
                'jumlah_barang'=> $request->jumlah_barang,
            ];

            // dd($data);
            
            $lastStock = Barang::where('id',$request->id_barang)->first()->stock;
            $lastStockOut = BarangKeluar::where('id',$request->id_barang_keluar)->first()->jumlah_barang;
            $edit = ($lastStock + $lastStockOut) - $request->jumlah_barang;
            // dd($lastStock, $lastStockOut,$edit, $request->jumlah_barang);
            Barang::where('id',$request->id_barang)->update(['stock'=>$edit]);
            
            BarangKeluar::where('id',$request->id_barang_keluar)->update($data);

            return back()->with('success','Data Berhasil Diedit');

        }

        $faktur = BarangKeluar::selectRaw('*, RIGHT (no_faktur, 1) as faktur')->orderBy('no_faktur', 'desc')->first()->faktur;
        $no_faktur = $faktur + 1;

        // dd($faktur);

        return view('proses.barangKeluar',[
            'title'=>'Data Barang Keluar',
            'data'=> Barang::join('barang_keluar','barang_keluar.id_barang','=','barang.id')
            ->select('barang.nama_barang','barang.kode_barang','barang_keluar.*','reseller.nama as nama_reseller')
            ->join('reseller','reseller.id','=','barang_keluar.id_reseller')
            ->get(),
            'barang'=> Barang::all(),
            'reseller'=> Reseller::all(),
            'no'=>1,
            'dateNow'=>Carbon::now()->format('d-m-Y H:i:s'),
            'faktur'=>'INV-'.Carbon::now()->format('dmY').'-00'.$no_faktur
        ]);
    }

    public function show($id)
    {
        $data = Barang::where('id',$id)->first();
        return response()->json([
            'status'=> 200,
            'data' => $data
        ]);
    }

    public function edit($id)
    {
        $data = Barang::join('barang_keluar','barang_keluar.id_barang','=','barang.id')
        ->where('barang_keluar.id',$id)->first();
        return response()->json([
            'status'=> 500,
            'data' => $data
        ]);
    }
    function destroy(Request $request){
        $kodeBarang = BarangKeluar::where('id',$request->id)->first()->id_barang;
        $jumlahBarang = BarangKeluar::where('id',$request->id)->first()->jumlah_barang;

        $lastStock = Barang::where('id',$kodeBarang)->first()->stock;
        $kurang = $lastStock + $jumlahBarang;
        Barang::where('id',$kodeBarang)->update(['stock'=>$kurang]);
        // dd($kurang, $kodeBarang);


        BarangKeluar::where('id',$request->id)->delete();


        return back()->with('success','Data Berhasil Dihapus');
    }
}
