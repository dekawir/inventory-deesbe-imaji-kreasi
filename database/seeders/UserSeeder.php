<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {


    $user = new User(array(
        // 'username'=>'manager',
        // 'password'=> Hash::make('manager'),
        // 'nama' => 'manager',
        // 'jabatan'=>'manager',
        // 'no_telp'=>'085792355688',
        // 'alamat'=>'Batubulan',
        // 'email'=>'manager@example.com'

        'username'=>'admin',
        'password'=> Hash::make('admin'),
        'nama' => 'admin',
        'jabatan'=>'admin',
        'no_telp'=>'085792355688',
        'alamat'=>'Batubulan',
        'email'=>'admin@example.com'
    ));

    $user->timestamps = false;
    $user->save();

    }
}
