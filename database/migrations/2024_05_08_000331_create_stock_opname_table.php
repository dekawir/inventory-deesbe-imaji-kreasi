<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('stock_opname', function (Blueprint $table) {
            $table->id();
            $table->string('id_user',5);
            $table->string('id_barang',5);
            $table->date('tgl_penyesuaian');
            $table->string('stock',20);
            $table->string('jumlah_sebenarnya',20);
            $table->string('keterangan');
            $table->string('status',20);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stock_opname');
    }
};
