<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('return_barang', function (Blueprint $table) {
            $table->id();
            $table->string('id_user',5);
            $table->string('id_reseller',5);
            $table->string('id_barang',5);
            $table->string('no_faktur',50);
            $table->date('tgl_return');
            $table->string('jumlah_barang',20);
            $table->string('keterangan');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('return_barang');
    }
};
