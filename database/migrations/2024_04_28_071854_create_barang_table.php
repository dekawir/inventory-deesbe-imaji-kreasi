<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->id();
            $table->string('id_user',5);
            $table->string('kode_barang',20);
            $table->string('kategori',50);
            $table->string('nama_barang',100);
            $table->string('gender',30);
            $table->string('warna',30);
            $table->string('size',50);
            $table->string('stock',20);
            $table->string('harga_jual',50);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('barang');
    }
};
