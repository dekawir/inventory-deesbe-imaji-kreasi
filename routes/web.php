<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\BarangKeluarController;
use App\Http\Controllers\BarangMasukController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\ResellerController;
use App\Http\Controllers\ReturnBarangController;
use App\Http\Controllers\StockOpnameController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth');
});

Route::get('/login',[AuthController::class, 'login']);
Route::post('/login',[AuthController::class, 'login'])->name('login');


Route::group(['middleware' => ['auth']], function () {
    Route::get('logout',[AuthController::class, 'logout'])->name('logout');

    Route::get('dashboard',[HomeController::class,'dashboard'])->name('dashboard');

    Route::get('admin',[HomeController::class,'admin'])->name('admin');
    
    Route::get('data-reseller',[ResellerController::class, 'index']);
    Route::post('data-reseller',[ResellerController::class, 'index']);
    Route::get('data-reseller-edit/{id}',[ResellerController::class,'edit']);
    Route::post('data-reseller-del',[ResellerController::class,'destroy'])->name('del.reseller');

    Route::get('data-barang',[BarangController::class, 'index']);
    Route::post('data-barang',[BarangController::class, 'index']);
    Route::get('data-barang-edit/{id}',[BarangController::class,'edit']);
    Route::post('data-barang-del',[BarangController::class,'destroy'])->name('del.barang');

    Route::get('data-barang-masuk',[BarangMasukController::class, 'index']);
    Route::post('data-barang-masuk',[BarangMasukController::class, 'index']);
    Route::get('data-barang-show/{id}',[BarangMasukController::class, 'show']);
    Route::get('data-barang-masuk-edit/{id}',[BarangMasukController::class,'edit']);
    Route::post('data-barang-masuk-del',[BarangMasukController::class,'destroy'])->name('del.barang_masuk');

    Route::get('data-barang-keluar',[BarangKeluarController::class, 'index']);
    Route::post('data-barang-keluar',[BarangKeluarController::class, 'index']);
    Route::get('data-barang-keluar-edit/{id}',[BarangKeluarController::class,'edit']);
    Route::post('data-barang-keluar-del',[BarangKeluarController::class,'destroy'])->name('del.barang_keluar');

    Route::get('data-return-barang',[ReturnBarangController::class, 'index']);
    Route::post('data-return-barang',[ReturnBarangController::class, 'index']);
    Route::get('data-return-barang-edit/{id}',[ReturnBarangController::class,'edit']);
    Route::post('data-return-barang-del',[ReturnBarangController::class,'destroy'])->name('del.barang_return');


    Route::get('stock-opname',[StockOpnameController::class, 'index']);
    Route::post('stock-opname',[StockOpnameController::class, 'index']);
    Route::get('stock-opname-edit/{id}',[StockOpnameController::class,'edit']);
    Route::post('stock-opname-del',[StockOpnameController::class,'destroy'])->name('del.stock_opname');
    Route::post('stock-opname-approve',[StockOpnameController::class,'approve'])->name('approve');


    



});

Route::group(['middleware' => ['cekLogin:manager']], function () {
    Route::get('manager',[HomeController::class,'manager'])->name('manager');

    Route::get('data-user',[UserController::class, 'index']);
    Route::post('data-user',[UserController::class, 'index']);
    Route::get('data-user-edit/{id}',[UserController::class,'edit']);
    Route::post('data-user-del',[UserController::class,'destroy'])->name('del.user');
    
    Route::get('l-data-stock-barang',[LaporanController::class, 'index']);
    Route::post('l-data-stock-barang',[LaporanController::class, 'index']);

    Route::get('l-barang-masuk',[LaporanController::class, 'masuk']);
    Route::post('l-barang-masuk',[LaporanController::class, 'masuk']);

    Route::get('l-barang-keluar',[LaporanController::class, 'keluar']);
    Route::post('l-barang-keluar',[LaporanController::class, 'keluar']);

    Route::get('l-stock-opname',[LaporanController::class, 'stockOpname']);
    Route::post('l-stock-opname',[LaporanController::class, 'stockOpname']);

});
