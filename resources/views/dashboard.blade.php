@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/morris/morris.css') }}" />

        <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/animate/animate.compat.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css') }}/all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/boxicons/css') }}/boxicons.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css') }}/bootstrap-datepicker3.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/morris/morris.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/chartist/chartist.min.css') }}" />

@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <div class="row mb-3">
                <div class="col-xl-3">
                    <section class="card card-featured-left card-featured-quaternary">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-quaternary">
                                        <i class="fas fa-box"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Stok Tersedia</h4>
                                        <div class="info">
                                            <strong class="amount">{{ App\Models\Barang::count() }}</strong>
                                            {{-- <span class="text-primary"></span> --}}
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="{{ URL::to('data-barang') }}">(view all)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-3">
                    <section class="card card-featured-left card-featured-tertiary mb-3">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fas fa-upload"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Barang Keluar</h4>
                                        <div class="info">
                                            <strong class="amount">{{ App\Models\BarangKeluar::count() }}</strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="{{ URL::to('data-barang-keluar') }}">(view all)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-3">
                    <section class="card card-featured-left card-featured-secondary">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-secondary">
                                        <i class="fas fa-download"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Barang Masuk</h4>
                                        <div class="info">
                                            <strong class="amount">{{ App\Models\BarangMasuk::count() }}</strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="{{ URL::to('data-barang-masuk') }}">(view all)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                
                @if(Auth::user()->jabatan == 'manager')
                <div class="col-xl-3">
                    <section class="card card-featured-left card-featured-primary mb-3">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fas fa-user"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">User</h4>
                                        <div class="info">
                                            <strong class="amount">{{ App\Models\User::all()->count() }}</strong>
                                            {{-- <span class="text-primary">Kepala Keluarga</span> --}}
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="{{ URL::to('data-user') }}">(view all)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                @endif
            </div>
        </div>
    </div>
    @if(Auth::user()->jabatan == 'manager')
    <div class="row">
        <div class="col-lg-6">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Barang Keluar</h2>
                    {{-- <p class="card-subtitle">You don't have to do much to get an attractive plot. Create a placeholder, make sure it has dimensions (so Flot knows at what size to draw the plot), then call the plot function with your data.</p> --}}
                </header>
                <div class="card-body">

                    <!-- Flot: Basic -->
                    <div class="chart chart-md" id="flotBasic"></div>
                    <script type="text/javascript">

                        var flotBasicData = [{
                            data: [
                                [1, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','1')->count() }}],
                                [2, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','2')->count() }}],
                                [3, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','3')->count() }}],
                                [4, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','4')->count() }}],
                                [5, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','5')->count() }}],
                                [6, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','6')->count() }}],
                                [7, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','7')->count() }}],
                                [8, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','8')->count() }}],
                                [9, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','9')->count() }}],
                                [10, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','10')->count() }}],
                                [11, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','11')->count() }}],
                                [12, {{ App\Models\BarangKeluar::whereMonth('tgl_keluar','12')->count() }}],
                            ],
                            label: "Data Barang Keluar",
                            color: "#0088cc"
                        }, 
                        {
                            data: [
                                [1, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','1')->count() }}],
                                [2, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','2')->count() }}],
                                [3, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','3')->count() }}],
                                [4, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','4')->count() }}],
                                [5, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','5')->count() }}],
                                [6, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','6')->count() }}],
                                [7, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','7')->count() }}],
                                [8, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','8')->count() }}],
                                [9, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','9')->count() }}],
                                [10, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','10')->count() }}],
                                [11, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','11')->count() }}],
                                [12, {{ App\Models\BarangMasuk::whereMonth('tgl_masuk','12')->count() }}],
                            ],
                            label: "Data Barang Masuk",
                            color: "#2baab1"
                        }
                    ];

                        // See: js/examples/examples.charts.js for more settings.

                    </script>

                </div>
            </section>
        </div>
        <div class="col-lg-6">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Barang Keluar Masuk dan Return</h2>
                    {{-- <p class="card-subtitle">Donut Chart are functionally identical to pie charts.</p> --}}
                </header>
                <div class="card-body">

                    <!-- Morris: Donut -->
                    <div class="chart chart-md" id="morrisDonut"></div>
                    <script type="text/javascript">

                        var morrisDonutData = [{
                            label: "Barang Masuk",
                            value: {{ App\Models\BarangMasuk::count() }}
                        }, {
                            label: "Barang Keluar",
                            value: {{ App\Models\BarangKeluar::count() }}
                        }, {
                            label: "Barang Return",
                            value: {{ App\Models\ReturnBarang::count() }}
                        }];

                        // See: js/examples/examples.charts.js for more settings.

                    </script>

                </div>
            </section>
        </div>
        
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Data Kategori</h2>
                    {{-- <p class="card-subtitle">With the categories plugin you can plot categories/textual data easily.</p> --}}
                </header>
                <div class="card-body">

                    <!-- Flot: Bars -->
                    <div class="chart chart-md" id="flotBars"></div>
                    <script type="text/javascript">

                        var flotBarsData = [
                            ["Sandal flip-flop", {{ App\Models\Barang::where('kategori','Sandal flip-flop')->count() }}],
                            ["Sandal slide", {{ App\Models\Barang::where('kategori','Sandal slide')->count() }}],
                            ["Sandal wedges", {{ App\Models\Barang::where('kategori','Sandal wedges')->count() }}],
                            ["Sandal tali/gladiator", {{ App\Models\Barang::where('kategori','Sandal tali/gladiator')->count() }}],
                            ["Sandal platform", {{ App\Models\Barang::where('kategori','Sandal platform')->count() }}],
                            ["Sandal heels", {{ App\Models\Barang::where('kategori','Sandal heels')->count() }}],
                            ["Sandal slingback", {{ App\Models\Barang::where('kategori','Sandal slingback')->count() }}]
                        ];

                        // See: js/examples/examples.charts.js for more settings.

                    </script>

                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Barang keluar</h2>
                    {{-- <p class="card-subtitle">Stacked Bar Chart.</p> --}}
                </header>
                <div class="card-body">

                    <!-- Morris: Area -->
                    <div class="chart chart-md" id="morrisStacked"></div>
                    <script type="text/javascript">

                        var morrisStackedData = [
                            
                        @foreach($barang as $b)
                        {
                            y: '{{ $b->nama_barang }}',
                            a: {{ $b->jumlah }}
                        },
                        @endforeach
                        
                    
                    ];

                        // See: js/examples/examples.charts.js for more settings.

                    </script>

                </div>
            </section>
        </div>
        <div class="col-lg-6">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Data Reseller</h2>
                </header>
                <div class="card-body">
                    <!-- Morris: Area -->
                    <div class="chart chart-md chart-bar-stacked-sm my-3" id="revenueChart" style="height: 409px;"></div>
                    <script type="text/javascript">

                        var revenueChartData = [
                        @foreach($reseller as $b)
                        {
                            y: '{{ $b->nama }}',
                            a: {{ $b->jumlah }},
                        },
                        @endforeach
                    ];

                        // See: js/examples/examples.charts.js for more settings.

                    </script>
                </div>
            </section>
        </div>
    </div>
    @endif

    <!-- end: page -->
    
@endsection
@push('js')
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('vendor/bootstrapv5-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('vendor/snap.svg/snap.svg.js') }}"></script>
    <script src="{{ asset('vendor/liquid-meter/liquid.meter.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/data/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}"></script>
    <script src="{{ asset('vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}"></script>

    <!-- Specific Page Vendor -->
    <script src="{{ asset('vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('vendor/snap.svg/snap.svg.js') }}"></script>
    <script src="{{ asset('vendor/liquid-meter/liquid.meter.js') }}"></script>
    <script src="{{ asset('vendor/chartist/chartist.js') }}"></script>

    {{-- <script src="{{ asset('js/examples/examples.charts.js') }}"></script> --}}

    
@endpush

@push('example')
    <!-- Examples -->
    <script src="{{ asset('js/examples/examples.dashboard.js') }}"></script>
    <style>
        #ChartistCSSAnimation .ct-series.ct-series-a .ct-line {
            fill: none;
            stroke-width: 4px;
            stroke-dasharray: 5px;
            -webkit-animation: dashoffset 1s linear infinite;
            -moz-animation: dashoffset 1s linear infinite;
            animation: dashoffset 1s linear infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-b .ct-point {
            -webkit-animation: bouncing-stroke 0.5s ease infinite;
            -moz-animation: bouncing-stroke 0.5s ease infinite;
            animation: bouncing-stroke 0.5s ease infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-b .ct-line {
            fill: none;
            stroke-width: 3px;
        }

        #ChartistCSSAnimation .ct-series.ct-series-c .ct-point {
            -webkit-animation: exploding-stroke 1s ease-out infinite;
            -moz-animation: exploding-stroke 1s ease-out infinite;
            animation: exploding-stroke 1s ease-out infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-c .ct-line {
            fill: none;
            stroke-width: 2px;
            stroke-dasharray: 40px 3px;
        }

        @-webkit-keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @-moz-keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @-webkit-keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @-moz-keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @-webkit-keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }

        @-moz-keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }

        @keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }
    </style>
    {{-- <script src="{{ asset('js/examples/examples.charts.js')}}"></script> --}}
<script>
    (function() {

		if( $('#flotBasic').get(0) ) {
			var plot = $.plot('#flotBasic', flotBasicData, {
				series: {
					lines: {
						show: true,
						fill: true,
						lineWidth: 1,
						fillColor: {
							colors: [{
								opacity: 0.45
							}, {
								opacity: 0.45
							}]
						}
					},
					points: {
						show: true
					},
					shadowSize: 0
				},
				grid: {
					hoverable: true,
					clickable: true,
					borderColor: 'rgba(0,0,0,0.1)',
					borderWidth: 1,
					labelMargin: 15,
					backgroundColor: 'transparent'
				},
				yaxis: {
					min: 0,
					max: 100,
					color: 'rgba(0,0,0,0.1)'
				},
				xaxis: {
					color: 'rgba(0,0,0,0.1)'
				},
				tooltip: true,
				tooltipOpts: {
					content: 'Bulan ke %x jumlah %y barang',
					shifts: {
						x: -60,
						y: 25
					},
					defaultTheme: false
				}
			});
		}
    
    if( $('#morrisDonut').get(0) ) {
		Morris.Donut({
			resize: true,
			element: 'morrisDonut',
			data: morrisDonutData,
			colors: ['#0088cc', '#734ba9', '#E36159']
	});
	}

    
    if( $('#flotBars').get(0) ) {
        var plot = $.plot('#flotBars', [flotBarsData], {
            colors: ['#8CC9E8'],
            series: {
                bars: {
                    show: true,
                    barWidth: 0.8,
                    align: 'center'
                }
            },
            xaxis: {
                mode: 'categories',
                tickLength: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderColor: 'rgba(0,0,0,0.1)',
                borderWidth: 1,
                labelMargin: 15,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: '%y',
                shifts: {
                    x: -10,
                    y: 20
                },
                defaultTheme: false
            }
        });
    }


	if( $('#ChartistHorizontalChart').get(0) ) {
        new Chartist.Bar('#ChartistHorizontalChart', {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            series: [
                [5,5,5,5,5,5,5],
            ]
        }, {
            seriesBarDistance: 100,
            // reverseData: true,
            horizontalBars: true,
            axisY: {
                offset: 70
            }
        });
		}
	})();

    if( $('#ChartistOverlappingBarsOnMobile').get(0) ) {
			var data = {
				labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				series: [
					[5, 4, 3, 7, 5, 10, 3, 4, 8, 10, 6, 8],
					[3, 2, 9, 5, 4, 6, 4, 6, 7, 8, 7, 4],
				]
			};

			var options = {
				seriesBarDistance: 10
			};

			var responsiveOptions = [
				['screen and (max-width: 640px)', {
					seriesBarDistance: 5,
					axisX: {
						labelInterpolationFnc: function(value) {
							return value[0];
						}
					}
				}]
			];

			new Chartist.Bar('#ChartistOverlappingBarsOnMobile', data, options, responsiveOptions);
		}

        if( $('#morrisStacked').get(0) ) {
		Morris.Bar({
			resize: true,
			element: 'morrisStacked',
			data: morrisStackedData,
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Series A'],
			barColors: ['#0088cc'],
			fillOpacity: 0.7,
			smooth: false,
			stacked: true,
			hideHover: true
		});
	}

    if( $('#revenueChart').get(0) ) {
		Morris.Bar({
			resize: true,
			element: 'revenueChart',
			data: revenueChartData,
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Series A'],
			barColors: ['#0088cc'],
			fillOpacity: 0.7,
			smooth: false,
			stacked: true,
			hideHover: true,
			grid: false
		});
	}

	
</script>

@endpush