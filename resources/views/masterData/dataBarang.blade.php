@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">{{ $title }}</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            {{-- <button id="modalForm"  class="btn btn-primary">Tambah <i class="fas fa-plus"></i></button> --}}
                            <a class="modal-with-form btn btn-primary" href="#modalForm">Tambah <i class="fas fa-plus"></i></a>

                            <!-- Modal Form -->
                            <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                                <form method="POST" action="">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">Registration Form</h2>
                                        </header>
                                        <div class="card-body">
                                            @csrf
                                            <div class="row form-group pb-3">
                                                <label class="col-form-label" for="">Kode barang :</label>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <input type="text" name="cat" class="form-control" id="cat" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <input type="text" name="urut" class="form-control" id="" value="{{ $urut }}" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <input type="text" name="gen" class="form-control" id="gen" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <input type="text" name="siz" class="form-control" id="size" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <input type="text" name="war" class="form-control" id="war" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Kategori :</label>
                                                <select id="kodeKategori" name="kategori" class="form-control">
                                                    <option value="">Choose...</option>
                                                    <option value="Sandal flip-flop">Sandal flip-flop</option>
                                                    <option value="Sandal slide">Sandal slide</option>
                                                    <option value="Sandal wedges">Sandal wedges</option>
                                                    <option value="Sandal tali/gladiator">Sandal tali/gladiator</option>
                                                    <option value="Sandal platform">Sandal platform</option>
                                                    <option value="Sandal heels">Sandal heels</option>
                                                    <option value="Sandal slingback">Sandal slingback</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Barang :</label>
                                                <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Gender :</label>
                                                <select id="kodeGender" name="gender" class="form-control">
                                                    <option value="">Choose...</option>
                                                    <option value="Pria">Pria</option>
                                                    <option value="Wanita">Wanita</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Warna :</label>
                                                <input type="text" name="warna" class="form-control" placeholder="Warna">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Size EU/UK/US :</label>
                                                <select id="kodeSize" name="size" class="form-control">
                                                    <option value="">Choose...</option>
                                                    <option value="35,5/2,5/5">35,5/2,5/5</option>
                                                    <option value="36/3/5,5">36/3/5,5</option>
                                                    <option value="36,5/3,5/6">36,5/3,5/6</option>
                                                    <option value="37,5/4/6,5">37,5/4/6,5</option>
                                                    <option value="38/4,5/7">38/4,5/7</option>
                                                    <option value="38,5/5/7,5">38,5/5/7,5</option>
                                                    <option value="39/5,5/8">39/5,5/8</option>
                                                    <option value="40/6/8,5">40/6/8,5</option>
                                                    <option value="40,5/6,5/9">40,5/6,5/9</option>
                                                    <option value="41/7/9,5">41/7/9,5</option>
                                                    <option value="42/7,5/10">42/7,5/10</option>
                                                    <option value="42,5/8/10,5">42,5/8/10,5</option>
                                                    <option value="43/8,5/11">43/8,5/11</option>
                                                    <option value="44/9/11,5">44/9/11,5</option>
                                                    <option value="44,5/9,5/12">44,5/9,5/12</option>
                                                </select>
                                            </div>
                                            {{-- <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="">Stock :</label>
                                                    <input type="text" name="stock" class="form-control" id="" placeholder="Stock" onkeypress="return hanyaAngka(event)">
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                <label for="">Harga Jual :</label>
                                                <div class="col-lg-12">
                                                    <input type="text" name="harga_jual" class="form-control" id="" placeholder="Harga Jual" onkeypress="return hanyaAngka(event)">
												</div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="add" value="add">Submit</button>
                                                    <button class="btn btn-default modal-dismiss">Cancel</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </form>
                            </div>

                           </div>
                    </div>
                    <script>
                        
                        document.getElementById('kodeKategori').onchange = function () {
                            var data = this.value;
                            if(data=='Sandal flip-flop'){
                                kategori='SFF';
                            }else if(data=='Sandal slide'){
                                kategori='SSS';
                            }else if(data=='Sandal wedges'){
                                kategori='SSW';
                            }else if(data=='Sandal tali/gladiator'){
                                kategori='STG';
                            }else if(data=='Sandal platform'){
                                kategori='SSP';
                            }else if(data=='Sandal heels'){
                                kategori='SSH';
                            }else if(data=='Sandal slingback'){
                                kategori='SLB';
                            }

                            // console.log(kategori);
                            $('#cat').val(kategori);

                        };
                        document.getElementById('kodeGender').onchange = function () {
                            var data = this.value;
                            if(data=='Pria'){
                                gender='ML';
                            }else{
                                gender='FM';
                            }
                            $('#gen').val(gender);
                            
                        };


                        document.getElementById('kodeSize').onchange = function () {
                            var data = this.value;
                            if(data=='35,5/2,5/5'){
                                size='355';
                            }else if(data=='36/3/5,5'){
                                size='360';
                            }else if(data=='36,5/3,5/6'){
                                size='365';
                            }else if(data=='37,5/4/6,5'){
                                size='375';
                            }else if(data=='38/4,5/7'){
                                size='380';
                            }else if(data=='38,5/5/7,5'){
                                size='385';
                            }else if(data=='39/5,5/8'){
                                size='390';
                            }else if(data=='40/6/8,5'){
                                size='400';
                            }else if(data=='40,5/6,5/9'){
                                size='405';
                            }else if(data=='41/7/9,5'){
                                size='410';
                            }else if(data=='42/7,5/10'){
                                size='420';
                            }else if(data=='42,5/8/10,5'){
                                size='425';
                            }else if(data=='43/8,5/11'){
                                size='430';
                            }else if(data=='44/9/11,5'){
                                size='440';
                            }else if(data=='44,5/9,5/12'){
                                size='445';
                            }
                            $('#size').val(size);
                            console.log(data);
                            
                        };



                        
                    </script>

                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">

                        <thead>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Kategori</th>
                                <th>Gender</th>
                                <th>Warna</th>
                                <th>Size</th>
                                <th>Stock</th>
                                <th>Harga Jual</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $u)
                            <tr>                                    
                                <td>{{ $u->kode_barang }}</td>
                                <td>{{ $u->nama_barang }}</td>
                                <td>{{ $u->kategori }}</td>
                                <td>{{ $u->gender }}</td>
                                <td>{{ $u->warna }}</td>
                                <td>{{ $u->size }}</td>
                                <td>{{ $u->stock }}</td>
                                <td>@currency($u->harga_jual)</td>
                                <td>
                                    @if($u->stock<1)
                                    <a  onclick="editbtn({{ $u->id }})" href="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></a>
                                    |
                                    @endif
                                    <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->

<!-- Modal delete -->
<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <form action="{{ route('del.barang') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin menghapus data ini?</p>
                        
                        <input type="hidden" id="delUser" name="id">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>

<!-- Modal Edit Form -->
<div id="editForm" class="modal-block modal-block-primary mfp-hide">
    <form method="POST" action="">
        <section class="card">
            <header class="card-header">
                <h2 class="card-title">Registration Form</h2>
            </header>
            <div class="card-body">
                @csrf
                <input type="hidden" name="id" id="editId">
                <div class="form-group">
                    <label>Kode Barang :</label>
                    <input type="text" name="kode_barang" id="kode_barang" class="form-control" placeholder="Kode Barang" readonly="readonly">
                </div>
                <div class="form-group">
                    <label for="">Kategori :</label>
                    <select id="kategori" name="kategori" class="form-control">
                        <option value="Sandal flip-flop">Sandal flip-flop</option>
                        <option value="Sandal slide">Sandal slide</option>
                        <option value="Sandal wedges">Sandal wedges</option>
                        <option value="Sandal tali/gladiator">Sandal tali/gladiator</option>
                        <option value="Sandal platform">Sandal platform</option>
                        <option value="Sandal heels">Sandal heels</option>
                        <option value="Sandal slingback">Sandal slingback</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Barang :</label>
                    <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang">
                </div>
                <div class="form-group">
                    <label for="">Gender :</label>
                    <select id="gender" name="gender" class="form-control">
                        <option value="Pria">Pria</option>
                        <option value="Wanita">Wanita</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Warna :</label>
                    <input type="text" name="warna" id="warna" class="form-control" placeholder="Warna">
                </div>
                <div class="form-group">
                    <label for="">Size EU/UK/US :</label>
                    <select id="size" name="size" class="form-control">
                        <option value="35,5/2,5/5">35,5/2,5/5</option>
                        <option value="36/3/5,5">36/3/5,5</option>
                        <option value="36,5/3,5/6">36,5/3,5/6</option>
                        <option value="37,5/4/6,5">37,5/4/6,5</option>
                        <option value="38/4,5/7">38/4,5/7</option>
                        <option value="38,5/5/7,5">38,5/5/7,5</option>
                        <option value="39/5,5/8">39/5,5/8</option>
                        <option value="40/6/8,5">40/6/8,5</option>
                        <option value="40,5/6,5/9">40,5/6,5/9</option>
                        <option value="41/7/9,5">41/7/9,5</option>
                        <option value="42/7,5/10">42/7,5/10</option>
                        <option value="42,5/8/10,5">42,5/8/10,5</option>
                        <option value="43/8,5/11">43/8,5/11</option>
                        <option value="44/9/11,5">44/9/11,5</option>
                        <option value="44,5/9,5/12">44,5/9,5/12</option>
                    </select>
                </div>
                {{-- <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="">Stock :</label>
                        <input type="text" name="stock" class="form-control" id="stock" placeholder="Stock" onkeypress="return hanyaAngka(event)">
                    </div>
                </div> --}}
                <div class="form-group">
                    <label for="">Harga Jual :</label>
                    <div class="col-lg-12">
                        <input type="text" name="harga_jual" class="form-control" id="harga_jual" placeholder="Harga Jual" onkeypress="return hanyaAngka(event)">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button class="btn btn-primary" type="submit" name="edit" value="edit">Submit</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </form>
</div>


{{-- Delete user --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delUser");
        input.value = id;
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>


<script>
    function editbtn(id){
        var input = document.getElementById("editId");
        input.value = id;

        $.ajax({
            type: "GET",
            url: "/data-barang-edit/"+id,
            success: function (response) {
                // var d = new Date(response.kk.tanggal_lahir);
                // var newDate = d.toString('dd/MM/yy');
                // var dateAr = response.kk.tanggal_lahir.split('-');
                // var newDate = dateAr[0];

                console.log(response);

                $('#kode_barang').val(response.data.kode_barang);
                $('#kategori').val(response.data.kategori).change;
                $('#nama_barang').val(response.data.nama_barang);
                $('#gender').val(response.data.gender).change;
                $('#warna').val(response.data.warna).change;
                $('#size').val(response.data.size).change;
                $('#stock').val(response.data.stock);
                $('#harga_jual').val(response.data.harga_jual);  
            }
        });
    }
</script>

@endsection



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>

@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>



<!-- Examples -->
<script>
(function($) {

'use strict';



$('.modal-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    modal: true
});

/*
Modal Dismiss
*/
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();

    const del = document.querySelector("#delete");

    // console.log(del.dataset);
    // console.log('haii');
});

/*
Form
*/
$('.modal-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    modal: true,

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
        beforeOpen: function() {
            if($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#name';
            }
        }
    }
});

/*
Ajax
*/
$('.simple-ajax-modal').magnificPopup({
    type: 'ajax',
    modal: true
});

}).apply(this, [jQuery]);
</script>
@endpush