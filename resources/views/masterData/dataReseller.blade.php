@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">{{ $title }}</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            {{-- <button id="modalForm"  class="btn btn-primary">Tambah <i class="fas fa-plus"></i></button> --}}
                            <a class="modal-with-form btn btn-primary" href="#modalForm">Tambah <i class="fas fa-plus"></i></a>

                            <!-- Modal Form -->
                            <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                                <form method="POST" action="">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">Registration Form</h2>
                                        </header>
                                        <div class="card-body">
                                            @csrf
                                            <div class="form-group">
                                                <label>Nama :</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Nama">
                                            </div>
                                            <div class="form-group">
                                                <label for="">No KTP :</label>
                                                <input type="text" name="no_ktp" class="form-control" id="" placeholder="No. KTP">
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Toko:</label>
                                                <input type="text" name="nama_toko" class="form-control" placeholder="Nama Toko">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Email :</label>
                                                <input type="email" name="email" class="form-control" id="" placeholder="Email">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="">No. Telp :</label>
                                                <input type="text" name="no_telp" class="form-control" id="" placeholder="No. Telp" onkeypress="return hanyaAngka(event)">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Alamat Toko :</label>
                                                <div class="col-lg-12">
                                                    <textarea name="alamat" class="form-control" rows="3" id="alamat"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="add" value="add">Submit</button>
                                                    <button class="btn btn-default modal-dismiss">Cancel</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </form>
                            </div>

                           </div>
                    </div>


                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">

                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Reseller</th>
                                <th>No. KTP</th>
                                <th>Nama Toko</th>
                                <th>Email</th>
                                <th>No Telp.</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $u)
                            <tr>                                    
                                <td>{{ $no++ }}</td>
                                <td>{{ $u->nama }}</td>
                                <td>{{ $u->no_ktp }}</td>
                                <td>{{ $u->nama_toko }}</td>
                                <td>{{ $u->email }}</td>
                                <td>{{ $u->no_telp }}</td>
                                <td>{{ $u->alamat }}</td>
                                <td>
                                    <a  onclick="editbtn({{ $u->id }})" href="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></a>
                                    |
                                    <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->

<!-- Modal delete -->
<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <form action="{{ route('del.reseller') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin menghapus data ini?</p>
                        
                        <input type="hidden" id="delUser" name="id">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>

<!-- Modal Edit Form -->
<div id="editForm" class="modal-block modal-block-primary mfp-hide">
    <form method="POST" action="">
        <section class="card">
            <header class="card-header">
                <h2 class="card-title">Registration Form</h2>
            </header>
            <div class="card-body">
                @csrf
                <input type="hidden" name="id" id="editId">
                <div class="form-group">
                    <label>Nama :</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama">
                </div>
                <div class="form-group">
                    <label for="">No KTP :</label>
                    <input type="text" name="no_ktp" class="form-control" id="no_ktp" placeholder="No. KTP" onkeypress="return hanyaAngka(event)">
                </div>
                <div class="form-group">
                    <label>Nama Toko:</label>
                    <input type="text" name="nama_toko" class="form-control" id="nama_toko" placeholder="Nama Toko">
                </div>
                <div class="form-group">
                    <label for="">Email :</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label for="">No. Telp :</label>
                    <input type="text" name="no_telp" class="form-control" id="no_telp" placeholder="No. Telp" onkeypress="return hanyaAngka(event)">
                </div>
                <div class="form-group">
                    <label for="">Alamat Toko :</label>
                    <div class="col-lg-12">
                        <textarea name="alamat" class="form-control" rows="3" id="alamat"></textarea>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button class="btn btn-primary" type="submit" name="edit" value="edit">Submit</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </form>
</div>


{{-- Delete user --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delUser");
        input.value = id;
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>

<script>
    function editbtn(id){
        var input = document.getElementById("editId");
        input.value = id;

        $.ajax({
            type: "GET",
            url: "/data-reseller-edit/"+id,
            success: function (response) {
                // var d = new Date(response.kk.tanggal_lahir);
                // var newDate = d.toString('dd/MM/yy');
                // var dateAr = response.kk.tanggal_lahir.split('-');
                // var newDate = dateAr[0];

                console.log(response);

                $('#nama').val(response.data.nama);
                $('#no_ktp').val(response.data.no_ktp);
                $('#nama_toko').val(response.data.nama_toko);
                $('#email').val(response.data.email);
                $('#no_telp').val(response.data.no_telp);
                $('#alamat').val(response.data.alamat);
  
            }
        });
    }
</script>

@endsection



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>

@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>



<!-- Examples -->
<script>
(function($) {

'use strict';



$('.modal-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    modal: true
});

/*
Modal Dismiss
*/
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();

    const del = document.querySelector("#delete");

    // console.log(del.dataset);
    // console.log('haii');
});

/*
Form
*/
$('.modal-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    modal: true,

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
        beforeOpen: function() {
            if($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#name';
            }
        }
    }
});

/*
Ajax
*/
$('.simple-ajax-modal').magnificPopup({
    type: 'ajax',
    modal: true
});

}).apply(this, [jQuery]);
</script>
@endpush