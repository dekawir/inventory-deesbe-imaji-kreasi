<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>{{ config('app.name') }} | {{ $title }}</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/animate/animate.compat.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/all.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />

		{{-- notif --}}
		<link rel="stylesheet" href="{{ asset('vendor/pnotify/pnotify.custom.css') }}" />
		

        @stack('css')

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('css/theme.css') }}" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('css/skins/default.css') }}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

		<!-- Head Libs -->
		<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>

	</head>
	<body class="loading-overlay-showing" data-loading-overlay>
		<div class="loading-overlay">
			<div class="bounce-loader">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="/dashboard" class="logo">
						<img src="{{ asset('logo/newlogo.png') }}" width="45" height="45"/>
					</a>

					<div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fas fa-bars" aria-label="Toggle sidebar"></i>
					</div>

				</div>

				<!-- start: search & user box -->
				<div class="header-right">

					{{-- <form action="pages-search-results.html" class="search nav-form">
						<div class="input-group">
							<input type="text" class="form-control" name="q" id="q" placeholder="Search...">
							<button class="btn btn-default" type="submit"><i class="bx bx-search"></i></button>
						</div>
					</form> --}}

					{{-- <span class="separator"></span> --}}

					{{-- <ul class="notifications">
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-bs-toggle="dropdown">
								<i class="bx bx-list-ol"></i>
								<span class="badge">3</span>
							</a>

							<div class="dropdown-menu notification-menu large">
								<div class="notification-title">
									<span class="float-end badge badge-default">3</span>
									Tasks
								</div>

								<div class="content">
									<ul>
										<li>
											<p class="clearfix mb-1">
												<span class="message float-start">Generating Sales Report</span>
												<span class="message float-end text-dark">60%</span>
											</p>
											<div class="progress progress-xs light">
												<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
											</div>
										</li>

										<li>
											<p class="clearfix mb-1">
												<span class="message float-start">Importing Contacts</span>
												<span class="message float-end text-dark">98%</span>
											</p>
											<div class="progress progress-xs light">
												<div class="progress-bar" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;"></div>
											</div>
										</li>

										<li>
											<p class="clearfix mb-1">
												<span class="message float-start">Uploading something big</span>
												<span class="message float-end text-dark">33%</span>
											</p>
											<div class="progress progress-xs light mb-1">
												<div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-bs-toggle="dropdown">
								<i class="bx bx-envelope"></i>
								<span class="badge">4</span>
							</a>

							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									<span class="float-end badge badge-default">230</span>
									Messages
								</div>

								<div class="content">
									<ul>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="img/!sample-user.jpg" alt="Joseph Doe Junior" class="rounded-circle" />
												</figure>
												<span class="title">Joseph Doe</span>
												<span class="message">Lorem ipsum dolor sit.</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle" />
												</figure>
												<span class="title">Joseph Junior</span>
												<span class="message truncate">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="img/!sample-user.jpg" alt="Joe Junior" class="rounded-circle" />
												</figure>
												<span class="title">Joe Junior</span>
												<span class="message">Lorem ipsum dolor sit.</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<figure class="image">
													<img src="img/!sample-user.jpg" alt="Joseph Junior" class="rounded-circle" />
												</figure>
												<span class="title">Joseph Junior</span>
												<span class="message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
											</a>
										</li>
									</ul>

									<hr />

									<div class="text-end">
										<a href="#" class="view-more">View All</a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<a href="#" class="dropdown-toggle notification-icon" data-bs-toggle="dropdown">
								<i class="bx bx-bell"></i>
								<span class="badge">3</span>
							</a>

							<div class="dropdown-menu notification-menu">
								<div class="notification-title">
									<span class="float-end badge badge-default">3</span>
									Alerts
								</div>

								<div class="content">
									<ul>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fas fa-thumbs-down bg-danger text-light"></i>
												</div>
												<span class="title">Server is Down!</span>
												<span class="message">Just now</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="bx bx-lock bg-warning text-light"></i>
												</div>
												<span class="title">User Locked</span>
												<span class="message">15 minutes ago</span>
											</a>
										</li>
										<li>
											<a href="#" class="clearfix">
												<div class="image">
													<i class="fas fa-signal bg-success text-light"></i>
												</div>
												<span class="title">Connection Restaured</span>
												<span class="message">10/10/2021</span>
											</a>
										</li>
									</ul>

									<hr />

									<div class="text-end">
										<a href="#" class="view-more">View All</a>
									</div>
								</div>
							</div>
						</li>
					</ul> --}}

					{{-- <span class="separator"></span> --}}

					<div id="userbox" class="userbox">
						<a href="#" data-bs-toggle="dropdown">
							{{-- <figure class="profile-picture">
								<img src="img/!logged-user.jpg" alt="Joseph Doe" class="rounded-circle" data-lock-picture="img/!logged-user.jpg" />
							</figure> --}}
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<span class="name">@auth{{ Auth::user()->nama }} @else Guest @endauth</span>
								<span class="role">@auth{{ Auth::user()->jabatan }} @else Guest @endauth</span>
							</div>

							<i class="fa custom-caret"></i>
						</a>

						<div class="dropdown-menu">
							<ul class="list-unstyled mb-2">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="bx bx-user-circle"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="bx bx-lock"></i> Lock Screen</a>
								</li>
								@auth
								<li>
									<a role="menuitem" tabindex="-1" href="{{ route('logout') }}"><i class="bx bx-power-off"></i> Logout</a>
								</li>
								
								@endauth
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">

				    <div class="sidebar-header">
				        <div class="sidebar-title">
				            Navigation
				        </div>
				        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
				            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
				        </div>
				    </div>

				    <div class="nano">
				        <div class="nano-content">
				            <nav id="menu" class="nav-main" role="navigation">

				                <ul class="nav nav-main">
				                    <li class="{{ ($title==="Dashboard"?'nav-active':'') }}">
				                        <a class="nav-link" href="dashboard">
				                            <i class="bx bx-home-alt" aria-hidden="true"></i>
				                            <span>Dashboard</span>
				                        </a>                        
				                    </li>
									
										
				                    <li class="nav-parent @if($title==="Data Reseller" ||$title==="Data Barang"||$title==="Data User") nav-expanded nav-active @endif">
				                        <a class="nav-link" href="#">
				                            <i class="bx bx-data" aria-hidden="true"></i>
				                            <span>Master Data</span>
				                        </a>
				                        <ul class="nav nav-children">
					   					@if(Auth::user()->jabatan == 'manager')
											<li>
												<a class="nav-link" href="data-user">
													Data User
												</a>
											</li>
				                            <li >
				                                <a class="" href="data-reseller">
				                                    Data Reseller
				                                </a>
				                            </li>
											@endif
                                            <li>
				                                <a class="nav-link" href="data-barang">
				                                    Data Barang
				                                </a>
				                            </li>
				                        </ul>
				                    </li>
				                    <li class="nav-parent @if($title==="Data Barang Masuk" ||$title==="Data Barang Keluar"||$title==="Data Return Barang") nav-expanded nav-active @endif">
				                        <a class="nav-link" href="#">
				                            <i class="bx bx-cog" aria-hidden="true"></i>
				                            <span>Proses</span>
				                        </a>
				                        <ul class="nav nav-children">
				                            <li >
				                                <a class="" href="data-barang-masuk">
				                                    Data Barang Masuk
				                                </a>
				                            </li>
                                            <li>
				                                <a class="nav-link" href="data-barang-keluar">
				                                    Data Barang Keluar
				                                </a>
				                            </li>
                                            <li>
				                                <a class="nav-link" href="data-return-barang">
				                                    Data Return Barang
				                                </a>
				                            </li>
				                        </ul>
				                    </li>
                                    <li class="{{ ($title==="Stock Opname"?'nav-active':'') }}">
				                        <a class="nav-link" href="stock-opname">
											<i class="bx bx-box" aria-hidden="true"></i>
				                            <span>Stock Opname</span>
				                        </a>                        
				                    </li>
									@if(Auth::user()->jabatan == 'manager')

									<li class="nav-parent @if($title==="Laporan Stock Barang" ||$title==="Laporan Data Barang Masuk"||$title==="Laporan Data Barang Keluar"||$title==="Laporan Stock Opname") nav-expanded nav-active @endif">
										<a class="nav-link" href="#">
											<i class="bx bx-file" aria-hidden="true"></i>
											<span>Laporan</span>
										</a>
										<ul class="nav nav-children">
											<li >
												<a class="" href="l-data-stock-barang">
													Laporan Stock Barang
												</a>
											</li>
											<li>
												<a class="nav-link" href="l-barang-masuk">
													Laporan Data Barang Masuk
												</a>
											</li>
											<li>
												<a class="nav-link" href="l-barang-keluar">
													Laporan Data Barang Keluar
												</a>
											</li>
											<li>
												<a class="nav-link" href="l-stock-opname">
													Laporan Stock Opname
												</a>
											</li>
										</ul>
									</li>

									@endif
                                    
				                </ul>
				            </nav>

				        </div>

				        <script>
				            // Maintain Scroll Position
				            if (typeof localStorage !== 'undefined') {
				                if (localStorage.getItem('sidebar-left-position') !== null) {
				                    var initialPosition = localStorage.getItem('sidebar-left-position'),
				                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

				                    sidebarLeft.scrollTop = initialPosition;
				                }
				            }
				        </script>

				    </div>

				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>{{ $title }}</h2>

						<div class="right-wrapper text-end">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="bx bx-home-alt"></i>
									</a>
								</li>

								<li><span>{{ $title }}</span></li>

							</ol>
                            <li class="sidebar-right-toggle"></li>
							{{-- <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a> --}}
						</div>
					</header>

					<!-- start: page -->
                        @yield('content')
					<!-- end: page -->
				</section>
			</div>


		</section>

		<!-- Vendor -->
		<script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
		<script src="{{ asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
		<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
		<script src="{{ asset('vendor/common/common.js') }}"></script>
		<script src="{{ asset('vendor/nanoscroller/nanoscroller.js') }}"></script>
		<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.js') }}"></script>
		<script src="{{ asset('vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>		
		
        <!-- Specific Page Vendor -->
		{{-- notif --}}
		<script src="{{ asset('vendor/pnotify/pnotify.custom.js') }}"></script>

        @stack('js')

		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('js/theme.js') }}"></script>

		<!-- Theme Custom -->
		<script src="{{ asset('js/custom.js') }}"></script>

		<!-- Theme Initialization Files -->
		<script src="{{ asset('js/theme.init.js') }}"></script>


		
		@stack('example')

		{{-- js validasi hanya angka --}}
		<script>
			function hanyaAngka(evt) {
			  var charCode = (evt.which) ? evt.which : event.keyCode
			   if (charCode > 31 && (charCode < 48 || charCode > 57))
		
				return false;
			  return true;
			}
		</script>

		<!-- Notification -->
		<script >
			(function($){
				@if(session('default'))
					new PNotify({
						title: 'Regular Notice',
						text: '{{ session('default') }}',
						type: 'custom',
						addclass: 'notification-primary',
						icon: 'fab fa-twitter'
					});
			
				@endif

				@if(session('success'))
					new PNotify({
						title: 'Sukses',
						text: '{{ session('success') }}',
						type: 'success'
					});
				@endif

				@if(session('errors'))
				@foreach(session('errors')->all() as $message)
					new PNotify({
						title: 'Error',
						text: '{{ $message }}',
						type: 'error'
					});
				@endforeach
				@endif

				@if(session('info'))
					new PNotify({
						title: 'Informasi',
						text: '{{ session('info') }}',
						type: 'info'
					});
				@endif
				

				@if(session('fail'))
					new PNotify({
						title: 'Kesalahan',
						text: '{{ session('fail') }}',
						addclass: 'notification-dark',
						icon: 'fas fa-user'
					});
				@endif
			}).apply(this, [jQuery]);
		</script>
		
	</body>
</html>