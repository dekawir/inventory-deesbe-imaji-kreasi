@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">{{ $title }}</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            {{-- <button id="modalForm"  class="btn btn-primary">Tambah <i class="fas fa-plus"></i></button> --}}
                            <a class="modal-with-move-anim ws-normal btn btn-primary" href="#modalForm">Tambah <i class="fas fa-plus"></i></a>

                            <!-- Modal Form -->
                            <div id="modalForm" class="modal-block modal-block-lg mfp-hide">
                                <form method="POST" action="">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">Registration Form</h2>
                                        </header>
                                        <div class="card-body">
                                            @csrf

                                            <div class="row form-group">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Tanggal Return Barang :</label>
                                                        <input type="text" name="tgl_return" class="form-control" id="tgl_return" value="{{ $dateNow }}" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Kode Barang :</label>
                                                        <select id="kodeBarang" name="id_barang" data-plugin-selectTwo class="form-control populate">
                                                            <option value="">Choose...</option>
                                                            @foreach($barang as $b)
                                                            <option value="{{ $b->id }}">{{ $b->kode_barang.' ('.$b->nama_barang.')' }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Nama Barang :</label>
                                                        <input type="text" name="" class="form-control" id="nama_barang" placeholder="Nama Barang" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Kategori :</label>
                                                        <input type="text" name="" class="form-control" id="kategori" placeholder="Kategori" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Gender :</label>
                                                        <input type="text" name="" class="form-control" id="gender" placeholder="Gender" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Warna :</label>
                                                        <input type="text" name="warna" class="form-control" id="warna" placeholder="Warna" readonly="readonly">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Stock :</label>
                                                        <input type="text" name="stock" class="form-control" id="stock" placeholder="Stock" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Jumlah Barang :</label>
                                                        <input type="text" name="jumlah_barang" class="form-control" id="" placeholder="Jumlah Barang" onkeypress="return hanyaAngka(event)">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">No. Faktur :</label>
                                                        <select id="no_faktur" name="no_faktur" data-plugin-selectTwo class="form-control populate">
                                                            <option value="">Choose...</option>
                                                            @foreach($faktur as $r)
                                                            <option value="{{ $r->no_faktur }}">{{ $r->no_faktur }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Nama Reseller :</label>
                                                        <select id="id_reseller" name="reseller" data-plugin-selectTwo class="form-control populate">
                                                            <option value="">Choose...</option>
                                                            @foreach($reseller as $r)
                                                            <option value="{{ $r->id }}">{{ $r->nama }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Keterangan :</label>
                                                <div class="col-lg-12">
                                                    <textarea name="keterangan" class="form-control" rows="4" id="keterangan"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="add" value="add">Submit</button>
                                                    <button class="btn btn-default modal-dismiss">Cancel</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </form>
                            </div>
                           </div>
                    </div>

                    <script>
                        document.getElementById('kodeBarang').onchange = function Barang(id){
                                // this.value = id;
                                var kode = this.value;
                                // console.log(kode);

                                $.ajax({
                                    type: "GET",
                                    url: "/data-barang-show/"+kode,
                                    success: function (response) {


                                        $('#nama_barang').val(response.data.nama_barang);
                                        $('#kategori').val(response.data.kategori);
                                        $('#gender').val(response.data.gender);
                                        $('#size').val(response.data.size);
                                        $('#warna').val(response.data.warna);
                                        $('#stock').val(response.data.stock);
                        
                                    }
                                });
                            }
                    </script>


                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">

                        <thead>
                            <tr>
                                <th>Tanggal Return Barang</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Kategori</th>
                                <th>Gender</th>
                                <th>Size</th>
                                {{-- <th>Jumlah Tercatat</th> --}}
                                <th>Jumlah Pengembalian</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $u)
                            <tr>                                    
                                <td>{{ $u->tgl_return }}</td>
                                <td>{{ $u->kode_barang }}</td>
                                <td>{{ $u->nama_barang }}</td>
                                <td>{{ $u->kategori }}</td>
                                <td>{{ $u->gender }}</td>
                                <td>{{ $u->size }}</td>
                                {{-- <td>{{ $u->stock }}</td> --}}
                                <td>{{ $u->jumlah_barang }}</td>
                                <td>{{ $u->keterangan }}</td>
                                <td>
                                    <div class="btn-group-vertical flex-wrap">
                                        <a  onclick="editbtn({{ $u->id }})" href="#editForm" class="dropdown-item text-1 btn btn-sm modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></a>
                                        <a onclick="del({{ $u->id }})" href="#modalAnim" class="dropdown-item text-1 btn btn-sm modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->

<!-- Modal delete -->
<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <form action="{{ route('del.barang_return') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin menghapus data ini?</p>
                        
                        <input type="hidden" id="delUser" name="id">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>

<!-- Modal Edit Form -->
<div id="editForm" class="modal-block modal-block-primary mfp-hide">
    <form method="POST" action="">
        <section class="card">
            <header class="card-header">
                <h2 class="card-title">Registration Form</h2>
            </header>
            <div class="card-body">
                @csrf
                <input type="hidden" name="id" id="id">
                <div class="row form-group">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Tanggal Return :</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="fas fa-calendar-alt"></i>
                                </span>
                                <input type="text" name="tgl_return" id="tgl_return" data-plugin-datepicker class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Kode Barang :</label>
                            <input type="text" name="kode" class="form-control" id="kode_barang" placeholder="Nama Barang" readonly="readonly">
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Nama Barang :</label>
                            <input type="text" name="" class="form-control" id="nama_barang" placeholder="Nama Barang" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Kategori :</label>
                            <input type="text" name="" class="form-control" id="kategori" placeholder="Kategori" readonly="readonly">
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Gender :</label>
                            <input type="text" name="" class="form-control" id="gender" placeholder="Gender" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Warna :</label>
                            <input type="text" name="warna" class="form-control" id="warna" placeholder="Warna" readonly="readonly">
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Stock :</label>
                            <input type="text" name="stock" class="form-control" id="stock" placeholder="Stock" readonly="readonly">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Jumlah Barang :</label>
                            <input type="text" name="jumlah_barang" class="form-control" id="jumlah_barang" placeholder="Jumlah Barang" onkeypress="return hanyaAngka(event)">
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">No. Faktur :</label>
                            <input type="text" name="no_faktur" class="form-control" id="no_faktur" placeholder="No. Faktur">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Nama Reseller :</label>
                            <select id="id_reseller" name="reseller" data-plugin-selectTwo class="form-control populate">
                                <option value="">Choose...</option>
                                @foreach($reseller as $r)
                                <option value="{{ $r->id }}">{{ $r->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Keterangan :</label>
                    <div class="col-lg-12">
                        <textarea name="keterangan" class="form-control" rows="4" id="keterangan"></textarea>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button class="btn btn-primary" type="submit" name="edit" value="edit">Submit</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </form>
</div>


{{-- Delete user --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delUser");
        input.value = id;
        console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>

<script>
    function editbtn(id){
        // var input = document.getElementById("editId");
        // input.value = id;
        // var kode = this.value;
        // console.log(id);

        $.ajax({
            type: "GET",
            url: "/data-return-barang-edit/"+id,
            success: function (response) {
                console.log(response);


                var date = new Date(response.data.tgl_return);

                // Get year, month, and day part from the date
                var year = date.toLocaleString("default", { year: "numeric" });
                var month = date.toLocaleString("default", { month: "2-digit" });
                var day = date.toLocaleString("default", { day: "2-digit" });

                // Generate yyyy-mm-dd date string
                var formattedDate = month + "/" + day + "/" + year;

                $('#tgl_return').val(formattedDate);
                $('#id').val(response.data.id);
                $('#keterangan').val(response.data.keterangan);
                $('#no_faktur').val(response.data.no_faktur);
                $('#id_reseller').val(response.data.id_reseller).change();
                $('#jumlah_barang').val(response.data.jumlah_barang);
                $('#id_barang').val(response.data.id_barang).change();
                $('#jumlah_barang').val(response.data.jumlah_barang);
                $('#kode_barang').val(response.data.kode_barang);
                $('#nama_barang').val(response.data.nama_barang);
                $('#kategori').val(response.data.kategori);
                $('#gender').val(response.data.gender);
                $('#size').val(response.data.size);
                $('#warna').val(response.data.warna);
                $('#stock').val(response.data.stock);

            }
        });
        

    }
</script>

@endsection



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>

@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.modals.js') }}"></script>

<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>




<!-- Examples -->
<script>
(function($) {

'use strict';



$('.modal-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    modal: true
});

/*
Modal Dismiss
*/
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();

    const del = document.querySelector("#delete");

    // console.log(del.dataset);
    // console.log('haii');
});

/*
Form
*/
$('.modal-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    modal: true,

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
        beforeOpen: function() {
            if($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#name';
            }
        }
    }
});

/*
Ajax
*/
$('.simple-ajax-modal').magnificPopup({
    type: 'ajax',
    modal: true
});

}).apply(this, [jQuery]);
</script>
@endpush