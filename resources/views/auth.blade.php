<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Favicons -->
		<link href="{{ asset('logo/logo.png') }}" rel="icon">
		<link href="{{ asset('logo/logo.png') }}" rel="apple-touch-icon">
		
		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/animate/animate.compat.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/all.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" />

		{{-- notif --}}
		<link rel="stylesheet" href="{{ asset('vendor/pnotify/pnotify.custom.css') }}" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('css/theme.css') }}" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('css/skins/default.css') }}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

		<!-- Head Libs -->
		<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>

	</head>
	<body>
		<div>
			<h1 style="text-align: center">SISTEM INVENTORY PADA PT DEESBE IMAJI KREASI BERBASIS WEB</h1>

		</div>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<div class="panel card-sign">
					<img style="  display: block; margin-left: auto; margin-right: auto; width: auto;" src="{{ asset('logo/logo.png') }}" height="100" alt="" />
					<div class="card-title-sign mt-3 text-end">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="bx bx-user-circle me-1 text-6 position-relative top-5"></i> Sign In</h2>
					</div>
					<div class="card-body">
						<form action="{{ route('login') }}" method="post">
                            @csrf
							{{-- <div class="alert alert-warning alert-dismissible fade show" role="alert">
								<strong>Pemberitahuan</strong> Username atau Password salah !!! <a href="" class="alert-link"></a>
								<button type="button" class="btn-close" data-bs-dismiss="alert" aria-hidden="true" aria-label="Close"></button>

							</div> --}}
							<div class="form-group mb-3">
								<label>Username</label>
								<div class="input-group">
									<input name="username" type="text" value="{{ old('username') }}" class="form-control form-control-lg" />
									<span class="input-group-text">
										<i class="bx bx-user text-4"></i>
									</span>
								</div>
							</div>

							<div class="form-group mb-3">
								<div class="clearfix">
									<label class="float-left">Password</label>
									{{-- <a href="pages-recover-password.html" class="float-end">Lost Password?</a> --}}
								</div>
								<div class="input-group">
									<input name="password" type="password" class="form-control form-control-lg" />
									<span class="input-group-text">
										<i class="bx bx-lock text-4"></i>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										
									</div>
								</div>
								<div class="col-sm-4 text-end">
									<button type="submit" class="btn btn-primary mt-2">Sign In</button>
								</div>
							</div>


							{{-- <p class="text-center">Don't have an account yet? <a href="pages-signup.html">Sign Up!</a></p> --}}

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright {{ date('Y') }}. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
		<script src="{{ asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
		<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
		<script src="{{ asset('vendor/common/common.js') }}"></script>
		<script src="{{ asset('vendor/nanoscroller/nanoscroller.js') }}"></script>
		<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.js') }}"></script>
		<script src="{{ asset('vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>

		<!-- Specific Page Vendor -->
		{{-- notif --}}
		<script src="{{ asset('vendor/pnotify/pnotify.custom.js') }}"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('js/theme.js') }}"></script>

		<!-- Theme Custom -->
		<script src="{{ asset('js/custom.js') }}"></script>

		<!-- Theme Initialization Files -->
		<script src="{{ asset('js/theme.init.js') }}"></script>

		<!-- Notification -->
		<script >
			(function($){
				@error('default')
					new PNotify({
						title: 'Regular Notice',
						text: '{{ $message }}',
						type: 'custom',
						addclass: 'notification-primary',
						icon: 'fab fa-twitter'
					});
			
				@enderror

				@error('success')
					new PNotify({
						title: 'Sukses',
						text: '{{ $message }}',
						type: 'success'
					});
				@enderror

				@error('info')
					new PNotify({
						title: 'Informasi',
						text: '{{ $message }}',
						type: 'info'
					});
				@enderror

				@error('error')
					// new PNotify({
					// 	title: 'Error',
					// 	text: '{{ $message }}',
					// 	type: 'error'
					// });

					var stack_topleft = {"dir1": "down", "dir2": "right", "push": "top"};
					var stack_bottomleft = {"dir1": "right", "dir2": "up", "push": "top"};
					var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15};
					var stack_bar_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0};
					var stack_bar_bottom = {"dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0};

					var notice = new PNotify({
						title: 'Notification',
						text: '{{ $message }}',
						type: 'error',
						addclass: 'stack-bar-bottom',
						stack: stack_bar_bottom,
						width: "70%"
					}).apply(this, [jQuery]);
				@enderror

				@error('dark')
					new PNotify({
						title: 'Kesalahan',
						text: '{{ $message }}',
						addclass: 'notification-dark',
						icon: 'fas fa-user'
					});
				@enderror
			}).apply(this, [jQuery]);
		</script>
	</body>
</html>